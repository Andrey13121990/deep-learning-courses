def binary_classification_metrics(prediction, ground_truth):
    '''
    Computes metrics for binary classification

    Arguments:
    prediction, np array of bool (num_samples) - model predictions
    ground_truth, np array of bool (num_samples) - true labels

    Returns:
    precision, recall, f1, accuracy - classification metrics
    '''
    
    TP = FN = FP = TN = 0

    for predict, fact in zip(prediction, ground_truth):
        if fact == True:
            if predict == True:
                TP +=1
            else:
                FN +=1

        else:   
            if predict == True:
                FP +=1
            else:
                TN +=1        


    precision = (TP + TN) / (TP + TN + FP + FN)
    recall = TP / (TP + FP)
    accuracy = TP / (TP + FN)
    f1 = 2 * precision * recall / (precision + recall)
    

    return precision, recall, f1, accuracy


def multiclass_accuracy(prediction, ground_truth):
    '''
    Computes metrics for multiclass classification

    Arguments:
    prediction, np array of int (num_samples) - model predictions
    ground_truth, np array of int (num_samples) - true labels

    Returns:
    accuracy - ratio of accurate predictions to total samples
    '''
    TP = FN = 0

    for predict, fact in zip(prediction, ground_truth):
        if fact == predict:         
            TP +=1
        else:
            FN +=1
  

    accuracy = TP / (TP + FN)

    return accuracy
